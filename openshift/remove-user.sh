#!/bin/bash

USERS=$(<list-of-users>)
GROUPS=$(oc get groups -o custom-columns=:metadata.name --no-headers | grep "<your-group-name>")

for GROUP in $GROUPS; do
  for USER in $USERS; do
    oc adm groups remove-users $GROUP $USER
  done
done
